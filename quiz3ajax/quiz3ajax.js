const express = require('express');
const fs = require('fs');
const app = express();

const port = 3003;
const jsonDataFile = 'data.json';


var tripsArray = [];

function saveAllData() {
    var dataStr = JSON.stringify(tripsArray, null, " ");
    fs.writeFileSync(jsonDataFile, dataStr);
}

function loadAllData() {
    if (fs.existsSync(jsonDataFile)) {
        var dataStr = fs.readFileSync(jsonDataFile);
        tripsArray = JSON.parse(dataStr);
    }
}

loadAllData();



app.get('/add', (request, response) => {
    var id = request.query['id'] - 0;
    var passport = request.query['passport'];
    var code = request.query['code'];
    var city = request.query['city'];
     
    var dateDep = request.query['dateDep'];
    var dateRet = request.query['dateRet'];
    
  
    // TODO: verify all 3 parameters look valid
    if (id < 1 || passport.length < 1 ) {
        response.send("error");
        return;
    }
    
    
    var trip = {id: id, passport: passport, code: code, city: city, dateDep: dateDep, dateRet: dateRet};
    tripsArray.push(trip);
    saveAllData();
    response.send(`trip with id=${id} created`);
     
    
});

app.get('/delete', (request, response) => {
    var id = request.query['id'] - 0;
    for (var i = 0; i < tripsArray.length; i++) {
        if (tripsArray[i].id === id) {
            tripsArray.splice(i, 1);
            saveAllData();
            response.send(`trip with id=${id} deleted`);
            return;
        }
    }
    response.send(`error deleting trip, id=${id} not found`);
});


app.get('/getList', (request, response) => {
    var html = "";
    console.log("FETCHALL CALLED");
    for (var i = 0; i < tripsArray.length; i++) {
        var f = tripsArray[i];
        
        html += `<option value="${f.id}">${f.id}: ${f.passport} travels to ${f.city} from : ${f.dateDep} to : ${f.dateRet}</option>`;
        
    }
    response.send(html);
});



app.get('/getListTable', (request, response) => {
     
 
    var html = "" + `<tr><td>Airport</td><td>Quantity</td></tr>` + "\n" ;
    
    var tripsArrayStut = [];
    var tripsArrayStut2 = [];
    
    var currentNumber = 0;
    //var currentCode;
    
    for (var i = 0; i < tripsArray.length; i++) {
             
        if (tripsArrayStut.includes(tripsArray[i].code)) {
            console.log("this is in" + tripsArray[i].code);
            
        }
        else{
            console.log("create this" + tripsArray[i].code);
            
            tripsArrayStut.push(tripsArray[i].code);   
            
        }           
    } 
    
    for (var i = 0; i < tripsArrayStut.length; i++) {
        console.log(tripsArrayStut[i]);
        
        for (var j = 0; j < tripsArray.length; j++) {
            if (tripsArrayStut[i] === tripsArray[j].code) {
                currentNumber ++;
            }             
        }
        tripsArrayStut2.push({code: tripsArrayStut[i], number: currentNumber}); 
        //tripsArrayStut.push({code: tripsArrayStut[i], number: currentNumber}); 
        console.log(tripsArrayStut2[i]);
        currentNumber = 0;
      
    }
      
    for (var i = 0; i < tripsArrayStut2.length; i++) {
        
        var f = tripsArrayStut2[i];
        
        html += `<tr>
                    <td>${f.code}</td>
                    <td >${f.number}</td>
                 </tr>` + "\n";
    }
      
    response.send(html);
});



app.listen(port, (err) => {
    if (err) {
        return console.log('something bad happened', err);
    }

    console.log(`server is listening on ${port}`);
});
