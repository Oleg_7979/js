const express = require('express');
const app = express();
const port = 3000;

app.get('/', (request, response) => {
    response.send('Hello from Express!');
});

var count = 0;
app.get('/count', (request, response) => {
    response.send('<p>Count is <b>' + count++ + '</b></p>');
});

app.get('/genrand', (request, response) => {
    var min= request.query['min'];
    var max= request.query['max'];
    // TODO: generate random number
    var rand = 876;
    response.send("" + rand);
});


app.listen(port, (err) => {
    if (err) {
        return console.log('something bad happened', err);
    }

    console.log(`server is listening on ${port}`);
});